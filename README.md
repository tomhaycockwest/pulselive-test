# Pulselive Frontend Development Task | Tom Haycock-West

Install the dependencies and devDependencies and start the server.

```sh
cd src
npm install
npm start
```

As soon as you save a file it will compile and copy it into the `dist` directory.
The site will be accessible at the below location:

```sh
localhost:5000
```

#### Building for source

For production release:

```sh
npm run build
```

The project will build into the `dist` directory at the root of the project

To view the compiled build in a browser

```sh
npx serve ../dist
```
The site will be accessible at the below location:

```sh
localhost:5000
```

