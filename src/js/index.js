const data = require("../player-stats.json").players;

const POSITIONS = {
  D: "Defender",
  M: "Midfilder",
  F: "Striker",
};

// coordinates of club badges on the sprite sheet
const BADGE_COORDINATES = {
  1: { x: -100, y: -100 },
  11: { x: -800, y: -700 },
  12: { x: -600, y: -800 },
  21: { x: -500, y: -1000 },
  26: { x: 0, y: 0 },
};

function addListeners() {
  document
    .getElementById("player-list")
    .addEventListener("change", (event) => handleChange(event));
}

const handleChange = (event) => {
  const playerId = parseInt(event.target.value);

  getPlayerStats(playerId);
};

const getPlayerStats = (playerId) => {
  const activePlayer = data.find((item) => item.player.id === playerId);

  const { player, stats } = activePlayer;

  const badgeX = [BADGE_COORDINATES[player.currentTeam.id].x, "px"].join("");
  const badgeY = [BADGE_COORDINATES[player.currentTeam.id].y, "px"].join("");
  const badge = [badgeX, badgeY].join(" ");
  document.getElementById("badge").style.backgroundPosition = badge;

  const avatar = ["images/", "p", player.id, ".png"].join("");
  document.getElementById("avatar").src = avatar;

  const playerName = [player.name.first, player.name.last].join(" ");
  document.getElementById("name").innerText = playerName;

  const position = player.info.position;
  document.getElementById("position").innerText = POSITIONS[position];

  const appearances = getStat(stats, "appearances");
  document.getElementById("appearances").innerText = appearances;

  const goals = getStat(stats, "goals");
  document.getElementById("goals").innerText = goals;

  const assists = getStat(stats, "goal_assist");
  document.getElementById("assists").innerText = assists;

  const goalsPerMatch = (goals / appearances).toFixed(2);
  document.getElementById("goals-per-match").innerText = goalsPerMatch;

  const forwardPasses = getStat(stats, "fwd_pass");
  const backwardPasses = getStat(stats, "backward_pass");
  const totalPasses = forwardPasses + backwardPasses;
  const minutesPlayed = getStat(stats, "mins_played");
  const passesPerMinute = (totalPasses / minutesPlayed).toFixed(2);
  document.getElementById("passes-per-minute").innerText = passesPerMinute;
};

const getStat = (stats, name) => {
  const result = stats.find((stat) => stat.name === name)?.value;
  return result === undefined ? "n/a" : result;
};

const populatePlayerList = () => {
  const players = data.map(({ player }) => {
    const node = document.createElement("option");
    const name = [player.name.first, player.name.last].join(" ");
    const nameTextNode = document.createTextNode(name);

    node.value = player.id;
    node.appendChild(nameTextNode);

    document.getElementById("player-list").appendChild(node);
  });
};

const init = () => {
  addListeners();
  populatePlayerList();
  getPlayerStats(4916); // set initial state to first player ID
};

document.addEventListener("DOMContentLoaded", init);
